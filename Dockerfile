FROM php:7.2-fpm

RUN apt-get update \
    && apt-get install -y \
    software-properties-common \
    git \
    vim \
    zip \
    curl \
    sudo \
    unzip \
    net-tools \
    iputils-ping \
    libicu-dev \
    libbz2-dev \
    libpng-dev \
    libjpeg-dev \
    libmcrypt-dev \
    libreadline-dev \
    libfreetype6-dev \
    #libpcre3-dev \
    libicu-dev postgresql-server-dev-* \
    g++



#curl -s http://getcomposer.org/installer | php


WORKDIR /tmp

#RUN \
#	set -eu; \
#	docker-php-ext-install pdo_pgsql; \
#	rm /usr/local/etc/php/conf.d/docker-php-ext-pdo_pgsql.ini; \
#	:
RUN pecl install redis && docker-php-ext-enable redis

RUN \
	set -eu; \
	curl -sS \
		-o composer-setup.php \
		https://getcomposer.org/installer \
	; \
	test \
		$( \
			openssl dgst -sha384 composer-setup.php \
			| tail -c97 \
		) \
		= "$(curl -sS https://composer.github.io/installer.sig)" \
	; \
	php composer-setup.php \
		--install-dir=/usr/local/bin \
		--filename=composer \
	; \
	rm composer-setup.php; \
	:

RUN \
	set -eu; \
  git clone https://github.com/jbboehr/php-psr.git; \
  cd php-psr && phpize && ./configure; \
  make && make test && make install; \
  :

RUN set -x && \
    NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) && \
    docker-php-ext-install -j${NPROC} \
    bz2 \
    intl \
#    iconv \
#    bcmath \
#    opcache \
#    calendar \
    mbstring \
     pdo_pgsql \
    zip

ARG phalcon_version='4.0.4'

RUN \
	set -eu; \
	mkdir phalcon; \
	curl -sSL https://codeload.github.com/phalcon/cphalcon/tar.gz/v"${phalcon_version}" \
	| tar -xz --strip-components=1 -C phalcon; \
	cd phalcon/build; \
	./install; \
	cd /tmp; \
	rm -rf \
		phalcon \
		phalcon.tar.gz \
	; \
	:

#RUN phalcon commands help 
RUN echo extension=psr.so | tee -a /usr/local/etc/php/conf.d/docker-php-ext-10-psr.ini
RUN echo extension=phalcon.so  | tee -a /usr/local/etc/php/conf.d/docker-php-ext-20-phalcon.ini
RUN php --ini

WORKDIR /usr/src
RUN git clone git://github.com/phalcon/phalcon-devtools.git
RUN cd phalcon-devtools/ && ln -s $(pwd)/phalcon /usr/bin/phalcon && chmod ugo+x /usr/bin/phalcon && composer install
RUN phalcon commands help

# RUN postgres -V


#alias phalcon=~/Projects/melodimedia/mock/phalcon-devtools/phalcon 

#RUN docker-php-ext-install \
#    bz2 \
#    intl \
#    iconv \
#    bcmath \
#    opcache \
#    calendar \
#    mbstring \
#    pdo_mysql \
#     pdo_pgsql \
#    zip



## 5. composer
#COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# COPY . /usr/src/app
#CMD [ "php", "./index.php" ]

WORKDIR /usr/src/app
RUN php -m | grep phalcon
RUN phalcon project app

