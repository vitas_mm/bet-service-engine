'use strict';
const path = require('path');
const root = path.resolve('./')
const fs = require('fs');
const us = require('../string');

class Action
{
  constructor(controllersPath)
  {
    this.controllers = [];
    this.URLvsCon = {};

    this.recursiveFiles(controllersPath, controllersPath);

    // console.log(this.URLvsCon);
  }

  recursiveFiles(controllersPath, basePath)
  {
    const re = new RegExp(basePath);
    let URLbase = controllersPath.replace(re, '');
    // URLbase = URLbase.replace(/^\//, '');
    URLbase += '/';
    // console.log({ URLbase});
    fs.readdirSync(controllersPath).forEach(file => {
      const fullPath = path.join(controllersPath, file);
      const isDir = fs.lstatSync(fullPath).isDirectory()

      // console.log({ f:  `${controllersPath} ${file}`, isDir });
      if (!isDir) {

        try {
          const o = require(fullPath);
          const oType = Object.prototype.toString(o);
          const { name } = o.constructor;
          if (name !== 'Function') return;

          const i = new o;
          const URLName = us.camelToSplit(i.constructor.name);
          const idxURLName = URLName.split('-');
          if (idxURLName.length < 2) return;         
          if (idxURLName[idxURLName.length - 1] != 'controller') return;         

          idxURLName.splice(idxURLName.length - 1, 1);
          const URLConstructor = idxURLName.join('-');
          const methods = Object.getOwnPropertyNames(o.prototype);

          methods.forEach(method => {
            if (!method.match(/Action$/)) return
            let URLAction = us.camelToSplit(method);  
            const idxAction = URLAction.split('-');
            idxAction.splice(idxAction.length - 1, 1);
            URLAction = idxAction.join('-');

            if (URLAction == 'index')
              return this.URLvsCon[URLbase + URLConstructor] = { Controller: o, method , fullPath };
     
            this.URLvsCon[URLbase + URLConstructor + '/' + URLAction] = { Controller: o, method, fullPath };
          });

          // console.log({ f: fullPath.toLowerCase(), i: Object.getOwnPropertyNames(o.prototype), URLConstructor });
        } catch (e) {
          console.log(e);
        }
        
        // console.log({ fullPath }); 
        return this.controllers.push(fullPath, basePath);
      }
      
      return this.recursiveFiles(fullPath, basePath);
    });
  }

  run(req, res, next)
  {
    let mode = 'json';
    let target = req.params[0] || req.baseUrl;
    target = (!target.match(/^\//)) ? '/' + target : target; 
    const idx = target.split('/');
    const URLMVC = (idx.length <= 1) ? idx[0] :  idx[0] + '/' + idx[1];

    if (this.URLvsCon[target]) {
      try {
        const { Controller, method } = this.URLvsCon[target];
        const i = new Controller(req, res, next);
        return Promise.resolve(i[method](mode)); 
      } catch (e) {
        console.error(e);
        return Promise.reject(e);
      }
    }

    // console.error(this.URLvsCon[URLMVC], URLMVC, idx);
    // console.error(this.URLvsCon);
    // console.error(req.baseUrl);
    return Promise.reject('Error binding Controllers Action.');
  }
}

module.exports = Action;
