'use strict';
const Abstract = require('./abstract');

class Client extends Abstract
{
  constructor()
  {
    super();
  }

  getSchema()
  {
    return {
      'cols': {
        'slug': {
          'type': 'string',
          'notNull': 1
        },
        'name': {
          'type': 'string',
          'notNull': 1
        },
        'raw': {
          'type': 'json',
          'notNull': 0
        },
      },
      'index': {
        'unique': [
          ['slug', 'name']  
        ]
      }
    }
  }
}

module.exports = Client;
