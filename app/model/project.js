'use strict';
const Abstract = require('./abstract');

class Project extends Abstract
{
  constructor()
  {
    super();
  }

  getSchema()
  {
    return {
      'cols': {
        'project_slug': {
          'type': 'string',
          'notNull': 1
        },
        'client_slug': {
          'type': 'string',
          'notNull': 1
        },
        'gateway_name': {
          'type': 'string',
          'notNull': 0
        },
        'model_name': {
          'type': 'string',
          'notNull': 1
        },
        'RNG_name': {
          'type': 'string',
          'notNull': 0
        },
        'session_duration': {
          'type': 'number',
          'default': 120,
          'notNull': 0
        },
        'rules': {
          'type': 'object',
          'notNull': 1
        },
        'params': {
          'type': 'object',
          'notNull': 0
        },
        'raw': {
          'type': 'object',
          'notNull': 0
        },
        
      },
      'index': {
        'unique': [
          ['project_slug', 'client_slug']  
        ]
      }
    }
  }
}

module.exports = Project;
