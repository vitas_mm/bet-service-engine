const Abstract = require('./abstract');
const fs = require('fs');

class Install extends Abstract
{
  constructor()
  {
    super();
  }

  db()
  {
    const models = this.getModels();    
    
    return this.connect().then(({db, client}) => {
      models.forEach(Model => {
        console.log(Model);
        const model = new Model();
        const { name } = model.constructor;
        const collection = db.collection(name);
        const schema = model.getSchema();

        if (schema.index) {
          Object.keys(schema.index).forEach((k) => {
            const cols = schema.index[k];
            const index = {};

            index[k] = 1;
            console.log({index}) 
            cols.forEach(col => {
              return collection.createIndex(col, index).catch(e => console.error(e));
            });
          });
        }
      });
      client.close();
    });
  }

  getModels()
  {
    const ignore = ['abstract.js', 'install.js'];
    const models = []; 

    fs.readdirSync(__dirname).forEach(file => {
      if (ignore.indexOf(file) == -1) {
        models.push(require(__dirname + '/' + file));
      }   
    });

    return models;
  }
  
}

module.exports = Install;

