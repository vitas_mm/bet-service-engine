'use strict';

const MongoClient = require('mongodb').MongoClient;
const { ObjectID } = require('mongodb');

const url = 'mongodb://mongo:27017';
const DBName = 'bet_service_dev';

class Abstract
{
  constructor()
  {
    this.keys = ['_id', 'slug'];
    this.data = {};
  }
  
  getSchema()
  {
    return {
      'cols': {
        'raw': {
          'type': 'json',
          'notNull': 0
        },
      }
    };
  }

  connect()
  {
    return new Promise((resolve, reject) => {
      MongoClient.connect(url, {
          'auth': {
            'user': 'root',
            'password': 'example'
          }
        }, function(err, client) {

        const db = client.db(DBName);
        if (err) reject(err);
        else resolve({db, client});
      });
    });
  }

  setData(data = {})
  {
    const { cols } = this.getSchema();

    Object.keys(cols).forEach(key => {
      if (data[key] != undefined) {
        this.data[key] = this.formValue(key, data[key]);
      }
    });

    Object.keys(data).forEach(key => {
      if (!cols[key]) this.data[key] = data[key];
    });

    if (data._id) this.data._id = new ObjectID(data._id);

    return this;
  }


  setBulkData()
  {
    return this;
  }

  getData()
  {
    const { cols } = this.getSchema();

    Object.keys(cols).forEach(col => {
      if (this.data[col]) return;
      const { type, notNull } = cols[col];  

      if (notNull) throw new Error(Col `"${col}" is required`);

      this.data[col] = null;

      if (type === 'string') this.data[col] = '';      
      if (type === 'object') this.data[col] = {};      
      if (type === 'array') this.data[col] = [];      
      
    });

    // return { ...cols, ...this.data };
    return this.data;
  }

  validate()
  {
    const { cols } = this.getSchema();
   
    Object.keys(cols).forEach(col => {
      const rules = cols[col];

      if (rules.notNull && this.data[col] == undefined && rules.default) 
        return this.data[col] = rules.default;

      if (rules.notNull && (this.data[col] == undefined || this.data[col] == null))
        throw `Dev error. ${col} is required.`;        

      if (this.data[col] && rules.type && typeof this.data[col] != rules.type)
        throw `Dev error. ${col} expected type: ${rules.type}.`;        

      if (rules.enum && rules.enum.indexOf(this.data[col]) == -1)
        throw `Dev error. ${col} val is not part of enumerated values: ${rules.enum}.`;        
    }); 

    return this;
  }

  getCollection(db)
  {
    const { name } = this.constructor;
    return db.collection(name);
  }

  fetch(query = {}, projection = {})
  {
/*
myModel.find(filter)
            .limit(pageSize)
            .skip(skip)
            .sort(sort)
            .toArray(callback);
*/
    return this.DB().then(({ client, collection }) => {
      const cursor = collection.find(query);
      cursor.project(projection); 
      const result = cursor.toArray();  
      client.close();
      return result;
    });  
  }

  normaliseView(body)
  {
    const { cols } = this.getSchema();
    const projection = {}; 

    if (cols) {
      Object.keys(cols).forEach(col => {
        if (cols[col].sensitive == 1) projection[col] = 0;
      });
    }

    const query = { 
      $query: {}, 
      $orderby: { _id : -1 },
    };

    return this.fetch(body, query, projection).then(data => {
      return data; 
    });
  }

  remove(body, _id)
  {
    if (_id) {
      return this.DB().then(({ client, collection }) => {
        const result = collection.deleteOne({ _id: new ObjectID(_id) });  
        client.close();
        return result;
      });  
    }

    const { index } = this.getSchema();
    let query = {};

    if (index && index.unique && index.unique.length) {
      let flag = index.unique[0].length;

      index.unique[0].forEach(key => {
        if (body[key]) query[key] = body[key];
      });            

      if (Object.keys(query).length == index.unique[0].length) {
        return this.DB().then(({ client, collection }) => {
          const result = collection.deleteOne(query);  
          client.close();
          return result;
        });  
      }
    } 

    return Promise.reject();
  }

  formValue(key, val)
  {
    let value = val; 
    const { cols } = this.getSchema();
    const { type, notNull } = cols[key];

    if (!key && !notNull) return value;

    if (type === 'object' && type != typeof val) value = JSON.parse(val || '{}');

    return value;
  }

  DB()
  {
    return this.connect().then(({db, client}) => {
      const collection = this.getCollection(db);
      return {db, client, collection };
    });
  }

  save() {
    return this.DB().then(({ client, collection }) => {
      return collection.insertOne(this.validate().getData()).then(inserts => {
        client.close();
        if (inserts.result.ok == 1) return Promise.resolve({ data: inserts });
        else return Promise.reject(inserts);
      });
    });  
  }

  update(where)
  {
    return this.DB().then(({ client, collection }) => {
      return collection.replaceOne(where, this.getData()).then(inserts => {
        client.close();
        if (inserts.result.ok == 1 && inserts.modifiedCount == 1) return inserts;
        else return Promise.reject(inserts);
      });
    });
  }
}

module.exports = Abstract;


