'use strict';
const Abstract = require('./abstract');

class Reports extends Abstract
{
  constructor()
  {
    super();
  }

  getSchema()
  {
    return {
      'cols': {
        'project_slug': {
          'type': 'string',
          'notNull': 1
        },
        'client_slug': {
          'type': 'string',
          'notNull': 1
        },
        'adapter': {
          'type': 'string',
          'notNull': 1
        },
        'rules': {
          'type': 'object',
          'notNull': 1
        },
        'project': {
          'type': 'object',
          'notNull': 1
        },
        'raw': {
          'type': 'object',
          'notNull': 0
        },
        
      },
      'index': {
        'unique': [
          ['project_slug', 'client_slug', 'session_slug']  
        ]
      }
    }
  }
}

module.exports = Play;


