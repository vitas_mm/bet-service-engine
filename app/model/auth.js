'use strict';
const Abstract = require('./abstract');

class Auth extends Abstract
{
  constructor()
  {
    super();

    this.state = {};
  }

  getSchema()
  {
    return {
      'cols': {
        'name': {
          'type': 'string',
          'notNull': 1
        },
        'hashedPass': {
          'type': 'string',
          'notNull': 1,
          'sensitive': 1,
        },
        'salt': {
          'type': 'string',
          'notNull': 1,
          'sensitive': 1,
        },
        'permission': {
          'type': 'object',
          'notNull': 1,
          'default': {admin: {client: {}}},
          'recurrent': {
            'admin': {
              'client': {} 
            } 
          }
        },
        'adapter': {
          'type': 'string',
          'notNull': 1,
          'enum': ['basic', 'session', 'oauth']
        }
      },
      'index': {
        'unique': [
          ['adapter', 'name']  
        ]
      }
    }
  }
}

module.exports = Auth;
