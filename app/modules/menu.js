'use strict';

const menu = {
  main: {
    session: 1,
    group: 1, // clients group
    admin: {
      session: 0,
      group: 0,
      GB: {
        title: 'Admin',
        URL: '/admin/login'
      }
    },
    manage: {
      group: 2, // admin group
      GB: {
        title: 'Management',
        proxy: {
          title: 'Proxy server',
          URL: '/manage/proxy'
        },
        clients: {
          title: 'Clients & authentication',
          URL: '/manage/clients'
        }
      }
    },
    activity: {
      group: 2, // admin group
      GB: {
        title: 'Activity',
        log: {
          title: 'Access Log',
          URL: '/activity/access-log'
        },
        server: {
          title: 'Server state',
          URL: '/activity/server'
        }
      }
    },
    state: {
      expanded: true,
      GB: {
        title: 'Payment states',
        internal: {
          title: 'Internal reports',
          URL: '/status/:session-slug'
        },
        gateway: {
          title: 'Gateway reports',
          URL: '/status-gateway/:session-slug'
        }
      }
    },
    reports: {
      GB: {
        title: 'Reports',
        URL: '/reports/'
      }
    }   
  },
  top: {

  },
  bottom: {

  },
  right: {

  }
}

module.exports = menu;
