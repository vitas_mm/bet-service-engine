'use strict';
const { postPlain, get } = require('../libs/netcom');
class Rules
{
  constructor(totalTry, history)
  {
    this.totalTry = totalTry;
    this.history = history;
  }

    
  init(access_token)
  {
    return this._checkBalance(access_token).then(d => {
      const outcome = {
        ...d,
        ...this._getOutcomeConstruct(),
        description: 'Init the game'
      };
      return this._setGameId(access_token).then(newGame => {
        return { ...outcome, ...newGame };
      }); 
    });
  }

  /**
    Every users request is a Start position which evaluates: 
      * Total availtable ammount of tries   
  */
  start()
  {
    return Promise.resolve({
      ...this._getOutcomeConstruct(),
      description: "Define the odds and param/const/option to choose the win." 
       + "Example: " 
       + "key: rule_0 => value: win_odds_stake|1.5|param_stake"
       + "key: param_stake => value: [10, 50, 150, 200, 500]"
       + "OR"
       + "key: param_stake => value: [10...500]"
    });
  }

  finish()
  {
    let cashWon = 0;
    let cashStaked = 0;
    if (!this.history) return { init: 'fishing' }

    const { customerId, gameId, accessToken } = this.history[0][0];
    this.history.forEach(node => {
      node.forEach(({isWin, win, stake}) => {
        if (isWin) { cashWon += Number(win); }
        if (stake) { cashStaked += Number(stake); }
      });
    });

    return Promise.resolve({
      cashWon,
      cashStaked,
      customerId,
      accessToken,
      gameId,
      ...this._getOutcomeConstruct(),
      description: "End of game." 
    });
  }

  repeat(rule, total = 1)
  {
    this.totalTry += total;

    const outcome = this._getOutcomeConstruct();

    return Promise.resolve({
      ...outcome,
      description: "Define the odds and param/const/option to choose the win." 
       + "Example: " 
       + "key: rule_0 => value: win_odds_stake|1.5|param_stake"
       + "key: param_stake => value: [10, 50, 150, 200, 500]"
       + "OR"
       + "key: param_stake => value: [10...500]"
    });
  }

  win_odds_stake(rule, odds, stake)
  {
    let isWin = false;
    const { attributes } = rule;

    if (attributes["odds"].isConst && attributes["odds"].value[0] === odds) { isWin = true; }
    // if (attributes["odds"].isConst && attributes.odds.value.indexOf(odds) != -1) { isWin = true; }
    // if (attributes[odds].isConst && attributes.odds.value.indexOf(odds) != -1) { isWin = true; }

    if (isWin && attributes["stake"].isConst && attributes["stake"].value[0] !== stake) { isWin = false; }
    if (isWin && attributes["stake"].isInterval) { 
      const min = Number(attributes["stake"].value[0]); 
      const max = Number(attributes["stake"].value[1]); 
      if (Number(stake) < min || Number(stake) > max) { isWin = false; } 
    }
    if (isWin && attributes["stake"].isSerial) {
      if (attributes["stake"].value.indexOf(stake) == -1) { isWin = false; }
    } 

    const { accessToken, customerId, gameId } = this.history[0][0];
    return this._submitTransaction(accessToken, customerId, gameId, Number(stake), 0).then(transaction => {
      return Promise.resolve({
        rule, 
        odds: Number(odds), 
        stake: Number(stake), 
        math: 'win = odds * stake',
        win: odds * stake,
        isWin, 
        transaction: transaction,
        ...this._getOutcomeConstruct(),
        description: "Define the odds and param/const/option to choose the win." 
         + "Example: " 
         + "key: rule_0 => value: win_odds_stake|1.5|param_stake"
         + "key: param_stake => value: [10, 50, 150, 200, 500]"
         + "OR"
         + "key: param_stake => value: [10...500]"
      });
    });
  }

  getRules()
  {
    const rules = Object.getOwnPropertyNames(this.__proto__);
    const ignore = ['constructor', 'getRules', 'init', '_*'];
    const data = {};

    rules.forEach(rule => {
      const re = new RegExp('(^_*)');
      const isMatch = rule.match(re); 
       
      if (!!isMatch[0] || ignore.indexOf(rule) != -1) return;
  
      data[rule] = {};
    });

    return data;
  }

  _getOutcomeConstruct()
  {
    const { totalTry } = this;
    return {
      totalTry,
      description: "Rules construct description" 
    }
  }

  _checkBalance(accessToken)
  {
    const username = 'melodimedia';
    const password = 'exG1pVj@';
    const host = 'qa.gateway.playabet.co.ke';
    const path = '/api/customer/' + accessToken;
    const headers = {
      'Authorization': 'Basic ' + new Buffer(username + ':' + password).toString('base64')
    };

    return get(path, host, 443, {}, { headers });
  }

  _setGameId(accessToken)
  {
    const username = 'melodimedia';
    const password = 'exG1pVj@';
    const host = 'qa.gateway.playabet.co.ke';
    const path = '/api/Game';
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Basic ' + new Buffer(username + ':' + password).toString('base64')
    };

    let body = {
      accessToken,
      "gameRemoteId": "Game_js",   
      "gameProviderRef": "Melodi_Media",  
      "gameTypeRef": "Spin_Win",  
      "description": "games"
    };

    return postPlain(host, 443, path, body, { headers }).then(r => JSON.parse(r));
  }

  async _submitTransaction(accessToken, customerId, gameId, cashStaked, cashWon)
  {
    const username = 'melodimedia';
    const password = 'exG1pVj@';
    const host = 'qa.gateway.playabet.co.ke';
    const path = '/api/play';
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Basic ' + new Buffer(username + ':' + password).toString('base64')
    };

    var ts = Math.round((new Date()).getTime() / 1000); 
  
    let body = {
      // accessToken,
      // cashStaked: 0,
      // cashWon: 1,
      "bonusStaked": 0,
      "description": "virtualgame",
      gameId,
      "gameRemoteId": "1000",
      "gameProviderRef": "Melodi_Media",
      "gameTypeRef": "Spin_Win",
      "remoteRef": "jSP_358-" + ts + '-' + Math.round(Math.random() * 100),
      // "customerId": "254444555666",
      // "customerId": "254756939387"
      // customerId
    };

    if (cashWon == 0 && cashStaked == 0) {
      return this._checkBalance(accessToken);
    }

    if (cashWon > 0) {

      body.cashWon = cashWon;
      body.customerId = customerId;
    } else {

      body.cashStaked = cashStaked;
      body.accessToken = accessToken;
    }

    console.log({ body });
   
    return postPlain(host, 443, path, body, { headers }).then(r => JSON.parse(r));
  }
}

module.exports = Rules;
