const redis = require("redis");

/*



client.psubscribe("chan-2");
*/

class Redis
{
  constructor(port, host)
  {
    this.client = redis.createClient(port, host);

    this.client.on('connect', function() {
      console.log('Redis client connected');
    });

    this.client.on('error', function (err) {
      console.log('Something went wrong ' + err);
    });
  }

  onPublish(callback = () => {})
  {
    this.client.monitor(function (err, res) {
      console.log('Entering REDIS monitoring mode.', {err, res});
    });

    this.client.on('monitor', function (time, args) {

      if (args[0] === 'PUBLISH') callback(time, args);
    });
  }

}

module.exports = Redis;
