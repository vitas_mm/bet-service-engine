'use strict';
const path = require('path');
const express = require('express');
const router = express.Router();
const Action = require('./../libs/controller/action');

const controllersPath = path.join(__dirname, 'controllers');
const actions = new Action(controllersPath);

router.all('/*', function(req, res, next) {
  return actions.run(req, res, next).then(({data, mode}) => {
    mode = mode || 'json';
    if (mode === 'json') { 
      if (typeof data === 'object' && data.constructor.name === 'Promise') {
        return data.then(r => {
          res.json({success: 1, data: r});
        });
      }
      res.json({success: 1, data});
    }
  }).catch(e => {
    let err = typeof e === 'object' ? e.message : e; 
    let stack = typeof e === 'object' ? e.stack : ''; 
    try { err = JSON.parse(err) } catch (e) {} 
    res.json({ success: 0, err, stack });
  });
});

module.exports = router;
