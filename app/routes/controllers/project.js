'use strict';
const Controller = require('./');
const Model = require('./../../model/project');
const Rules = require('./../../modules/rules');

class ProjectController extends Controller
{
  constructor(...args)
  {
    super(...args);

    this.model = new Model();
    this.module = new Rules();
  }

  indexAction()
  {
    return { data: this.CRUD() }
  }  

  save()
  {
    const { rules, params } = this.getBodyRules();
    const model_name = this.model.constructor.name; 

    this.model.setData({ rules, params, model_name });   
    this.model.setData(this.body);   
    return this.model.save();
  }

  update()
  {
    const { client_slug, project_slug } = this.body;
    const { rules, params } = this.getBodyRules();
    const model_name = this.model.constructor.name; 

    this.model.setData({ rules, params, model_name }).setData(this.body)

    return this.model.fetch({ client_slug, project_slug })
      .then(r => !r.length ? Promise.reject(new Error('Project is not found.')) : r[0])
      .then(({ _id }) => this.model.update({ _id}) );
  }

  getBodyRules()
  {
    const keys = Object.keys(this.body);
    const methods = this.module.getRules();
    const identifiedMethods = [];
    const rules = {};
    const paramLib = {};
    const errors = {
      methods: [],
      params: [],
      trigger: [],
    };
    let integrity = true;
    
    this.getBodyParams(paramLib, this.body);   

    keys.forEach(key => {
      const match = key.match(/rule_(\d)/);
      if (!match) return;
      identifiedMethods.push(key); 
    });

    identifiedMethods.forEach(key => {
      const match = key.match(/rule_(\d)/);
      const position = match[1];
      const [ method, trigger ] = this.body[key].split('-');
      const formation = method.split('|');
      const params = formation[0].split('_');
      const rule = {
        position,
        method: formation[0],
        attributes: {},
        isMethodAvailable: methods[formation[0]] ? 1 : 0,
        trigger
      };   

      if (!methods[formation[0]]) errors.methods.push({msg: 'Is not available', position});

      if (!trigger) errors.trigger.push({msg: 'Missing', position});   
      if (trigger && trigger.match(/^rule_/)) {
        rule.trigger = trigger.split('_')[1];
        if (rule.trigger === position) errors.trigger.push({msg: 'Infinite loop', position});   
        if (rule.trigger >= identifiedMethods.length) errors.trigger.push({msg: 'Do not exist.', position});   
      }
 
      formation.forEach((v, i) => {
        if (i == 0) return;
        if (paramLib[v]) {
          return rule.attributes[params[i]] = paramLib[v];
        }

        const body = {};
        const name = 'tech' + params[i] + v.replace(/[^a-z0-9]/g, ':');
        body['param_' + name] = v; 

        this.getBodyParams(paramLib, body);   

        if (paramLib[name]) {
          return rule.attributes[params[i]] = paramLib[name];
        }
      });
  
      rules[position] = rule;
    });

    if (errors.methods.length || errors.params.length || errors.trigger.length) 
      throw new Error(JSON.stringify(errors));

    return { rules, params: paramLib };
  }

  getBodyParams(params, body)
  {
    const keys = Object.keys(body);

    keys.forEach(key => {
      const param = {
        isInterval: false,
        isSerial: false,
        isConst: false,
        value: []  
      } ;
      const match = key.match(/param_*/);
      if (!match) return;
      const strValue = body[key]; 
      const optInterval = strValue.split('\.\.\.');
      const optSerial = strValue.split(',');

      if (optSerial.length === 1 && optInterval.length === 1) {
        param.isConst = true;
        param.value = optSerial;
      }
      if (optInterval.length > 1) {
        param.isInterval = true;
        param.value = optInterval;
      }
      if (optSerial.length > 1) {
        param.isSerial = true;
        param.value = optSerial;
      }
       
      params[key.split('_')[1]] = param; 
    });
    return params;
  }

  
}

module.exports = ProjectController;
