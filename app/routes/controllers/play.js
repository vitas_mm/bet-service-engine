'use strict';
const Controller = require('./');
const ModelProject = require('./../../model/project');
const ModuleRules = require('./../../modules/rules');
const Model = require('./../../model/play');

class TestRNG
{
  run(len, outcome = null)
  {
    return Math.round(Math.random() * len);
  }
}
  
class TestWallet
{
  isFundAvailable(take, out = 1)
  {
    return {
      isFundAvailable: out * 1
    }
  }
}

class PlayController extends Controller
{
  constructor(...args)
  {
    super(...args);

    // static definition for now 
    // Use factory class to set in the future
    this.model = new Model();
    this.RNG = new TestRNG();
    this.walllet = new TestWallet();
  }

  indexAction()
  {
    return { data: this.CRUD(null, ['GET']) }
  }

  sessionAction()
  {
    const data = {}
    const { project_slug, client_slug, session_slug, access_token } = this.body;
    const query = { project_slug, client_slug, session_slug }; 

    return this.model.fetch(query).then(r => {
      if (r.length) return Promise.reject('Session used already');
      const projects = new ModelProject();
      return projects.fetch({ project_slug, client_slug });
    }).then(r => {
      if (r.length === 0) return Promise.reject(new Error('Project not  found'));

      return Promise.resolve(r[0]);
    }).then(project => {
      // const { project_slug, client_slug, session_slug } = this.body;
      const { model_name, gateway_name, RNG_name, total_try } = project;
      const query = { project_slug, client_slug, session_slug }; 
      const modelRules = new ModuleRules(total_try || 1);
      return modelRules.init(access_token).then(initRules => {
        const rules = {
          sequence: [[{
            position: 'init',
            ...initRules
          }]]
        };  
        const save = { 
          project_slug, 
          client_slug, 
          session_slug, 
          rules, 
          project,
          raw: {}
        }

        // console.log({ save })
        return this.model.setData(save).save()
        return { data: save }
      })
    });
  }

  async spinAction()
  {
    const data = {}
    const { project_slug, client_slug, session_slug } = this.body;
    const query = { project_slug, client_slug, session_slug }; 

    return this.model.fetch(query).then(r => {
      if (!r.length) return Promise.reject('Session is not initiated');
      const { sequence } = r[0].rules;
      const last = sequence[sequence.length - 1]; 
      const query = { project_slug, client_slug }; 
      const projects = new ModelProject();

      return Promise.resolve(r[0]);
    }).then((session) => {
      const { rules } = session.project; 
      const { sequence } = session.rules; 
      const last = sequence[sequence.length - 1];
      const keys = Object.keys(rules);
      const selected = [];
      const { totalTry } = last[last.length - 1];

      if (totalTry <= 0) { return { data: session }; }

      const moduleRule = new ModuleRules(totalTry - 1, session.rules.sequence);
      const random = this.getRandomNum(keys.length - 1); 
      const rule = rules[random];
      return this.triggerRule(rules, random, moduleRule, selected).then(async () => {

        session.rules.sequence.push(selected);

        if (selected[selected.length - 1].position === 'finish') {
          const moduleRule = new ModuleRules(0, session.rules.sequence);
          const finish = await moduleRule.finish(session.rules.sequence);
          selected[selected.length - 1] = { position: 'finish', ...finish} 
        }

        if (selected.length === 0) return { data: session }
        const finalResults = selected[selected.length - 1];

        if (finalResults.position === 'finish') {
          console.log('U r generous to:', finalResults);
          const moduleRule = new ModuleRules(0, session.rules.sequence);
          const { accessToken, customerId, gameId, cashStaked, cashWon } = finalResults;
          const transaction = await moduleRule._submitTransaction(accessToken, customerId, gameId, 0, cashWon)
          finalResults.transaction = transaction;
        } 

        // return { data: session} ;
        return this.model.setData(session).update({ _id: session._id })
          .then(() => Promise.resolve({ data: session }));
      });
    });
  }

  getRandomNum(len)
  {
    return this.RNG.run(len);
  }

  triggerRule(rules, idx, moduleRule, selected = [])
  {
    const rule = rules[idx];
    const params = [rule];

    Object.keys(rule.attributes).forEach((param) => {
      const schema = rule.attributes[param];
      params.push(this.body[param]);
    }); 
    
    return moduleRule[rule.method].apply(moduleRule, params).then(r => {
      selected.push({ position: rule.method, ...r }); 
      if (rule.trigger.match(/[0-9]/)) {
        return this.triggerRule(rules, rule.trigger, moduleRule, selected);
      }
      if (rule.trigger === 'start') {
        return selected.push({ position: 'start', ...moduleRule.start() });
      }
      if (rule.trigger === 'finish') {
        return selected.push({ position: 'finish', ...moduleRule.finish() }); 
      }
 
      return r;
    });
  }
}

module.exports = PlayController;



