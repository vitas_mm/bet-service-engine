'use strict';
const Controller = require('./');
const Model = require('./../../model/install');

class InstallController extends Controller
{
  constructor(...args)
  {
    super(...args);
    this.model = new Model(); 
  }

  indexAction()
  {
    this.model.db();
    return { data: 'install' }
  }  


}

module.exports = InstallController;

