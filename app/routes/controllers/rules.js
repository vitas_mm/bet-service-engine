'use strict';
const Controller = require('./');
const Module = require('./../../modules/rules');

class RulesController extends Controller
{
  constructor(...args)
  {
    super(...args);
    this.module = new Module(); 
  }

  indexAction()
  {
    return { data: this.module.getRules() }
  }  
}

module.exports = RulesController;

