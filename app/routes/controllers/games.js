'use strict';
const Controller = require('./');
const CRUD = require('./../../modules/crud');

class GamesController extends Controller
{
  constructor()
  {
    super();

    this.db = new CRUD();
  }

  randomNodeAction()
  {

    return {'data': 'test'}
  }

}

module.exports = GamesController;



