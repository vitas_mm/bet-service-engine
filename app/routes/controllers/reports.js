'use strict';
const Controller = require('./');
const Model = require('./../../model/play');

class ReportsController extends Controller
{
  constructor(...args)
  {
    super(...args);
    this.model = new Model();
  }

  indexAction()
  {
    return { data: this.CRUD(null, ['GET']) }

  }

  dependency() {

  }  
}

module.exports = ReportsController;
