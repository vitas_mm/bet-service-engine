'use strict';
const CRUD = require('./../../modules/crud');

class Controller
{
  constructor(req, res, next)
  {
    if (!req) return this; 
    this.params = req.params;
    this.query = req.query;
    this.body = req.body;
    this.method = req.method;

    this.db = new CRUD();
  }
  
  CRUD()
  {
    const err = 'Method was not recognised.';

    switch(this.method) {
      case 'GET':
        return this.list();
      case 'POST':
        return this.save();
      case 'PUT':
        return this.update();
      case 'DELETE':
        return this.remove();
      default:
        throw new Error(err);
    } 

    return Promise.reject(new Error(err));
  }

  list()
  {
    return this.model.fetch({});
  }

  save()
  {
    return this.db.save();
  }

  update()
  {
    return this.db.update();
  }

  remove()
  {
    const { _id } = this.query;
    return this.model.remove(this.body, _id);
  }
}

module.exports = Controller;

