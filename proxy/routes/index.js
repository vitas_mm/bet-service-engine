// const httpProxy = require('http-proxy');
const express = require('express');
const router = express.Router();
const Controller = require('./controllers/');

const d = new Date();
const copyDate = d.getFullYear();
const title = "Payments Reports"

/*
const proxyOptions = {

};
const  proxy = httpProxy.createProxyServer(proxyOptions);
*/

router.get('/*', function(req, res, next) {
  const c = new Controller(req, res)

  console.log('INDEX'); 

  res.render('index', {c, title, copyDate });
});

module.exports = router;
