'use strict';

class PubSub
{
  constructor(adapter)
  {
    this.adapter = adapter;
  }

  onPublish(callback)
  {
    return this.adapter.onPublish(callback);
  }
}

module.exports = PubSub;
