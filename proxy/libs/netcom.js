'use strict';

const https = require('https');
const http = require('http');

const post = (host, port = null, path, form = {}, ext = {}, isSSL = true) => {
  const body = JSON.stringify(form)
  if (!port) port = isSSL ? 443 : 80;
 
  const headers = {
    'Content-Type': 'application/json',
    'Cache-Control': 'no-cache',
    'Content-Length': body.length,
    'Host': host + ':' + port,
  }

  if (ext.headers) {
    Object.assign(headers, ext.headers);
  }
  const options = {
    host, port, path, 
    method: 'POST',
    headers
  };
  const agentOptions = { 
    rejectUnauthorized: false, 
    requestCert: true,
  };

  if (isSSL) options.agent = new https.Agent(agentOptions);

  return new Promise((resolve, reject) => {
    const protocol = isSSL ? https : http;
    const req = protocol.request(options, function(res) {
      const output = [];
      res.on('data', data => {
        output.push(data);
      })
      res.on('end', () => {
        resolve(output.toString())
      });
      res.on('error', err => {
        reject(err)
      });
    });
    req.write(body);
    req.end();
  });
};

// eslint-disable-next-line no-unused-vars
const get = function(path, host, port, param = {}, ext = {}) {
  const headers = {
    'Accept': 'application/json',
    'Host': serialiseHost(host, port)
  };

  Object.assign(headers, ext.headers);

  const options = {
    host, port, path, headers
  };
  const agentOptions = { 
    rejectUnauthorized: false, 
    requestCert: true,
  };
  options.agent = new https.Agent(agentOptions);

  return new Promise((resolve, reject) => {
    const req = https.request(options, function(res) {
      const output = [];
      res.on('data', data => {
        output.push(data);
      })
      res.on('end', () => {
        if (res.statusCode === 500) {
          return resolve({})
          // return reject({ message: 'Error handling respond' }, res, output)
        }
        try {
          resolve(JSON.parse(output.join(' ')))
        } catch(e) {
          reject(e, res, output)
        } 
      });
      res.on('error', err => {
        reject(err);
      });
    });
    req.end();
  })
};

module.exports = { post, get };
