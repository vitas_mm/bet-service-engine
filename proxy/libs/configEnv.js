const fs = require('fs');
const dotenv = require('dotenv');

class ConfigEnv {

  constructor(stage) {
    this.stage = stage || '';
    this.flatENV = {};
    this.file = '.env';
  }

  flatten(obj, path = '') {
    // eslint-disable-next-line no-useless-escape
    if (!(obj instanceof Object)) return {[path.replace(/\_$/g, '')]:obj};

    return Object.keys(obj).reduce((output, key) => {
        return obj instanceof Array ? 
             {...output, ...this.flatten(obj[key], path +  '[' + key + '].')}:
             {...output, ...this.flatten(obj[key], path + key + '_')};
    }, {});
  }

  flattenVerbose(obj, path = '', pref = '', end = '') {
    if (!(obj instanceof Object)) {
      // eslint-disable-next-line no-useless-escape
      path = path.replace(/\_$/g, '');
      return { [path] : `${pref}_${path}_${end}`.toUpperCase() };
    }

    return Object.keys(obj).reduce((output, key) => {
      return {...output, ...this.flattenVerbose(obj[key], path + key + '_', pref, end)};
    }, {});
  }

  getENV() {
    let conf = {}

    if (fs.existsSync(this.file)) {
      conf = dotenv.parse(fs.readFileSync(this.file));
    }

    return conf;
  }

  ENVToJSON(config) {
    const env = this.getENV();

    const recConfig = (config, keys, value) =>  {
      if (keys.length > 1) {
        config[keys[0]] = recConfig(config[keys[0]], keys.splice(1), value);
        return config;
      }
      if (keys.length == 1) {
        config[keys[0]] = value;
      }

      return config;
    };

    Object.keys(env).forEach(k => {
      const keys = k.split('_');
      config = recConfig(config, keys, env[k]);
    });

    return this;
  }

  JSONToENV(config, prefix, end) {
    this.flatENV = this.flatten(config);
    this.flatKUB = this.flattenVerbose(config, '', prefix, end);
    return this;
  }

  saveTemplate(path, callback) {
    return this.save(path, callback, '.template', this.flatKUB)
  }

  saveExample(path, callback) {
    return this.save(path, callback, '.example', this.flatENV)
  }

  save(path, callback = () => {}, end = '', data) {
    let file = this.file;
    let content = '';

    file += (this.stage) ? '.' + this.stage : '';
    file += end;     

    Object.keys(data).forEach(k => {
      content += k + '=' + data[k] + "\n"
    });

    fs.writeFile(path + '/' + file, content, callback);

    return {
      path, file, content
    }
  }
  
  serialisePubSub(config, path) {
    const { prefix, projectName } = config;
    const { topic, sub } = path;
    path.topic = [prefix, projectName, topic].join('/');
    path.sub = [prefix, projectName, sub].join('/');
  }
}

module.exports = ConfigEnv
